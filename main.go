package main

import (
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"io/ioutil"
	"log"
	"os"

	"github.com/google/uuid"
	"github.com/square/go-jose"
)

func main() {
	contentOfPublicKeyFile, err := ioutil.ReadFile("public_key.pem")
	if err != nil {
		log.Fatalln("error reading public key pem file:", err)
	}

	block, _ := pem.Decode(contentOfPublicKeyFile)
	if block == nil {
		log.Fatalln("error decoding public key pem file content: nil block")
	}

	publicKey, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		log.Fatalln("error parsing public key pem file into public key:", err)
	}

	jwkSet := jose.JsonWebKeySet{
		Keys: []jose.JsonWebKey{
			jose.JsonWebKey{
				Key:       publicKey,
				KeyID:     uuid.New().String(),
				Algorithm: "RS256",
				Use:       "sig",
			},
		},
	}

	publicKeyJWKFile, err := os.OpenFile("public_key.jwk", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0444)
	if err != nil {
		log.Fatalln("error opening file for writing:", err)
	}
	defer publicKeyJWKFile.Close()

	if err = json.NewEncoder(publicKeyJWKFile).Encode(jwkSet); err != nil {
		log.Fatalln("error encoding JSON Web Key and writing it into .jwk file", err)
	}
}
